from flask import request

from settings import VERIFY_TOKEN, MOOD_REQUEST


def validate_token(token):
    if token == VERIFY_TOKEN:
        if request.args.get("hub.challenge"):
            return request.args["hub.challenge"]
    return "Token not valid"


def validate_user_message(message):
    return (
        message.get("message")
        and message["message"].get("text")
        and not message["message"].get("is_echo")
    )


def is_mood_request(text):
    return text == MOOD_REQUEST
