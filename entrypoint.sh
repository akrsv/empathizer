#!/bin/bash

set -e

flask db init && flask db migrate && flask db upgrade || flask db upgrade
gunicorn -b 0.0.0.0:5000 app:app