FROM python:3.8.0-alpine

COPY . /app
WORKDIR /app
RUN \
 apk add --no-cache bash && \
 apk add --no-cache postgresql-libs && \
 apk add --no-cache --virtual .build-deps gcc musl-dev postgresql-dev
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
RUN python -m nltk.downloader vader_lexicon stopwords punkt

EXPOSE 5000
CMD ["/bin/bash", "entrypoint.sh"]