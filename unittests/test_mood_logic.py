import pytest

from db.models import MoodEnum
from logic.mood_logic import MoodLogic


@pytest.fixture
def mood_logic():
    return MoodLogic()


@pytest.mark.parametrize(
    "message, expected",
    [
        ("The sun is shining", "The sun shining"),
        ("I hate this weather", "I hate weather"),
    ],
)
def test_remove_stop_words(mood_logic, message, expected):
    assert mood_logic.remove_stop_words(message) == expected


@pytest.mark.parametrize(
    "message, expected",
    [
        ("I love this weather", MoodEnum.positive),
        ("I hate this weather", MoodEnum.negative),
        ("I am tired", MoodEnum.negative),
        ("I need coffee", MoodEnum.neutral),
    ],
)
def test_get_mood_from_sentence(mood_logic, message, expected):
    assert mood_logic.get_mood_from_sentence(message) == expected
