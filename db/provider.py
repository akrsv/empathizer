from sqlalchemy import desc

from db.models import Mood


class MoodProvider:
    def __init__(self, db):
        self.db = db

    def get_or_create_default(self, user_id):
        mood = (
            Mood.query.filter(Mood.user_id == user_id).order_by(desc(Mood.date)).first()
        )
        if not mood:
            mood = self.insert({"user_id": user_id})

        return mood

    def insert(self, data):
        mood = Mood(**data)
        self.db.session.add(mood)
        self.db.session.commit()

        return mood
