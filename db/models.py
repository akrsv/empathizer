import datetime
import enum

from db import db


class MoodEnum(enum.Enum):
    positive = "positive"
    neutral = "neutral"
    negative = "negative"


class Mood(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.String)
    mood = db.Column(db.Enum(MoodEnum), server_default=MoodEnum.neutral.value)
    date = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
