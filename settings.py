FB_API_URL = "https://graph.facebook.com/v2.6/me/messages"
PAGE_ACCESS_TOKEN = ""
VERIFY_TOKEN = ""

POSTGRES_USER = "test"
POSTGRES_PASSWORD = "test"
POSTGRES_DB_NAME = "test"

DATABASE_URI = (
    f"postgresql://{POSTGRES_USER}:{POSTGRES_PASSWORD}@postgres:5432/{POSTGRES_DB_NAME}"
)

MOOD_REQUEST = "mood"

POSITIVE_RESPONSES = [
    "I know, it's great!",
    "I like it when you're in a good mood ^_^",
    "If you’re happy, I am happy",
    "So glad to hear that <3",
    "Wonderful :)",
]
NEUTRAL_RESPONSES = ["Ok", "Interesting", "Hm", "Mhm", "Allright"]
NEGATIVE_RESPONSES = [
    "I completely understand how you feel",
    "I feel so bad about what you wrote",
    "It doesn't sound good :(",
    "Poor you :'(",
    "I'm sorry to hear that",
]
