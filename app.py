from flask import Flask, request
from flask_migrate import Migrate

from db import db
from db.provider import MoodProvider
from helpers import validate_token
from logic.empathizer_logic import EmpathizerLogic
from logic.mood_logic import MoodLogic

app = Flask(__name__)
app.config.from_object("config.Config")
migrate = Migrate(app, db)

db.init_app(app)


@app.route("/", methods=["GET", "POST"])
def receive_message():
    if request.method == "GET":
        return validate_token(request.args.get("hub.verify_token"))

    if request.method == "POST":
        empathizer_logic = EmpathizerLogic(
            mood_logic=MoodLogic(), mood_provider=MoodProvider(db)
        )
        empathizer_logic.process_message(request.json)
        return "ok"


if __name__ == "__main__":
    app.run(host="0.0.0.0")
