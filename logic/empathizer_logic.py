import requests

from helpers import is_mood_request, validate_user_message
from settings import FB_API_URL, PAGE_ACCESS_TOKEN


class EmpathizerLogic:
    def __init__(self, mood_logic, mood_provider):
        self.mood_logic = mood_logic
        self.mood_provider = mood_provider

    def process_message(self, request):
        payload = request["entry"][0]["messaging"]
        for message in payload:
            if not validate_user_message(message):
                return

            user_id = message["sender"]["id"]
            text = message["message"]["text"]

            if is_mood_request(text):
                mood_obj = self.mood_provider.get_or_create_default(user_id)
                response = mood_obj.mood.value
            else:
                mood = self.mood_logic.get_mood_from_sentence(text)
                response = self.mood_logic.choose_response(mood)

                self.mood_provider.insert({"user_id": user_id, "mood": mood})

            self.respond(user_id, response)

    @staticmethod
    def respond(user_id, text):
        payload = {
            "message": {"text": text},
            "recipient": {"id": user_id},
            "notification_type": "regular",
        }
        auth = {"access_token": PAGE_ACCESS_TOKEN}
        response = requests.post(FB_API_URL, params=auth, json=payload)

        return response.json()
