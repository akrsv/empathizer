import random

from nltk.corpus import stopwords
from nltk.sentiment.vader import SentimentIntensityAnalyzer
from nltk.tokenize import word_tokenize

from db.models import MoodEnum
from settings import NEGATIVE_RESPONSES, NEUTRAL_RESPONSES, POSITIVE_RESPONSES

responses = {
    MoodEnum.positive: POSITIVE_RESPONSES,
    MoodEnum.neutral: NEUTRAL_RESPONSES,
    MoodEnum.negative: NEGATIVE_RESPONSES,
}


class MoodLogic:
    def __init__(self):
        self.analyzer = SentimentIntensityAnalyzer()

    def get_mood_from_sentence(self, message):
        filtered_sentence = self.remove_stop_words(message)
        score = self.analyzer.polarity_scores(filtered_sentence)

        if score["compound"] > 0.0:
            mood = MoodEnum.positive
        elif score["compound"] < 0.0:
            mood = MoodEnum.negative
        else:
            mood = MoodEnum.neutral

        return mood

    @staticmethod
    def choose_response(mood):
        return random.choice(responses[mood])

    @staticmethod
    def remove_stop_words(message):
        word_tokens = word_tokenize(message)
        stop_words = set(stopwords.words())

        filtered_sentence = " ".join(
            [word for word in word_tokens if not word in stop_words]
        )

        return filtered_sentence
